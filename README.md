# Social Login

The Social Login is a basic web application that demonstrates how to implement login functionality using social media platforms such as Google, Microsoft, and GitHub. The application allows users to authenticate themselves through their existing accounts on these platforms and showcases basic user data retrieved from the respective social media APIs. User sessions are maintained securely with JWT tokens sent through HTTP-only cookies. Additionally, the application includes a rate limiter to API requests.

## Table of Contents

- [Features](#features)
- [Technologies Used](#technologies-used)
- [Installation](#installation)

## Features

- **Social Login:** Users can log in to the application using their Google, Microsoft, or GitHub accounts.
- **User Data Display:** Upon successful login, the application displays basic user information retrieved from the authenticated social media account.
- **Authentication Flow:** The app demonstrates the OAuth authentication flow for each social media platform.
- **JWT Session:** User sessions are maintained with JWT tokens, which are securely stored in HTTP-only cookies.
- **Simple UI:** The user interface is minimalistic and easy to navigate, focusing on the login functionality and displaying user data.

## Technologies Used

- **Frontend:** HTML, CSS, JavaScript (React.js with TypeScript), Mantine
- **Backend:** Node.js (Express.js) with TypeScript, PrismaORM
- **Authentication:** Passport OAuth 2.0 (Google, Microsoft, GitHub), JWT (JSON Web Tokens)
- **Database:** PostgreSQL

## Installation

Clone the repository:

```bash
git clone https://gitlab.com/MilanVujakovic/social-login-integration.git
```

##### Backend

Inside backend folder:

1. Install dependencies:

   ```bash
   npm install
   ```

2. Create .env file by creating a copy of .env-template and changing values
3. Create Client id and secret by registering app:

   #### Google:

   ```
   https://console.cloud.google.com/apis/dashboard
   ```

   ```
   Homepage url: http://localhost:3000/
   Callback url: http://localhost:8000/api/auth/google/callback
   ```

   #### Microsoft:

   ```
   https://entra.microsoft.com/#view/Microsoft_AAD_RegisteredApps/ApplicationsListBlade/quickStartType~/null/sourceType/Microsoft_AAD_IAM
   ```

   ```
   Supported account types: Personal Microsoft accounts only
   Homepage url: http://localhost:3000/
   Callback url: http://localhost:8000/api/auth/microsoft/callback
   ```

   #### GitHub:

   ```
   https://github.com/settings/apps
   ```

   ```
   Homepage url: http://localhost:3000/
   Callback url: http://localhost:8000/api/auth/github/callback
   ```

4. Install PostgreSQL, create database and add parameters to .env

5. Setup Prisma

   ```
   npm i -g prisma
   npx prisma migrate dev --name init
   ```

6. Run
   ```
   npm run dev
   ```

##### Frontend

1. Install dependencies:

   ```bash
   npm install
   ```

2. Create .env file by creating a copy of .env-template and changing values

3. Run
   ```
   npm run dev
   ```
