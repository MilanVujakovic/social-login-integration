import Header from "../../components/Header/Header";
import UserInfo from "../../components/UserInfo/UserInfo";
import styles from "./Dashboard.module.css";
import { API_LOGOUT, API_USER_DATA, LOGIN_PAGE } from "../../common/routes";
import useGetData from "../../hooks/useGetData";
import { Box, LoadingOverlay } from "@mantine/core";
import { useCallback, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { UserData } from "../../types/User";

const Dashboard = () => {
  const data = useGetData<UserData>(
    import.meta.env.VITE_API_URL + API_USER_DATA
  );

  const [loading, setLoading] = useState(false);

  const navigate = useNavigate();

  const handleLogout = useCallback(async () => {
    try {
      setLoading(true);

      const response = await axios.post(
        import.meta.env.VITE_API_URL + API_LOGOUT,
        {},
        {
          withCredentials: true,
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (!(response.status === 200)) {
        throw new Error("Failed to logout");
      }
      // const data = await response.json();
      navigate(LOGIN_PAGE);
    } catch (error) {
      console.error("Error:", error);
    } finally {
      setLoading(false);
    }
  }, [navigate]);

  return (
    <Box pos="relative" style={{ height: "100dvh" }}>
      <LoadingOverlay
        visible={loading}
        zIndex={1000}
        overlayProps={{ blur: 2 }}
      />
      <div className={styles.wrapper}>
        {data ? (
          <>
            <Header data={data} onLogout={handleLogout} />
            <UserInfo data={data} />
          </>
        ) : null}
      </div>
    </Box>
  );
};

export default Dashboard;
