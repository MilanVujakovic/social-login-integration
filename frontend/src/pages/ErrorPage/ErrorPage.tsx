import { Container, Title } from "@mantine/core";
import classes from "./ErrorPage.module.css";

const ErrorPage = () => {
  return (
    <div className={classes.root}>
      <Container>
        <Title className={classes.title}>Something went wrong...</Title>
      </Container>
    </div>
  );
};

export default ErrorPage;
