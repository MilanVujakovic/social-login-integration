import { Text, Paper, Group, Center } from "@mantine/core";
import GoogleButton from "../../components/SignInButtons/GoogleButton/GoogleButton";
import Logo from "../../assets/Logo";
import styles from "./Login.module.css";
import { useCallback } from "react";
import {
  API_GITHUB_AUTH,
  API_GOOGLE_AUTH,
  API_MICROSOFT_AUTH,
} from "../../common/routes";
import MicrosoftButton from "../../components/SignInButtons/MicrosoftButton/MicrosoftButton";
import GitHubButton from "../../components/SignInButtons/GitHubButton/GitHubButton";

const Login = () => {
  // State to track loading state

  const handleGoogleClick = useCallback(() => {
    window.location.href = import.meta.env.VITE_API_URL + API_GOOGLE_AUTH;
  }, []);

  const handleMicrosoftClick = useCallback(() => {
    window.location.href = import.meta.env.VITE_API_URL + API_MICROSOFT_AUTH;
  }, []);

  const handleGitHubClick = useCallback(() => {
    window.location.href = import.meta.env.VITE_API_URL + API_GITHUB_AUTH;
  }, []);

  return (
    <Center
      style={{
        background: "linear-gradient(120deg, #8db8ab, #ffffff)",
        minHeight: "100dvh",
      }}
    >
      <Paper
        radius="md"
        p="xl"
        withBorder
        w={500}
        className={`${styles.paper} ${styles.mobilePaper}`}
      >
        <Logo width={100} height={100} />
        <Text size="xl" fw={500}>
          Log in
        </Text>
        <Group w="100%" mb="md" mt="md">
          <GoogleButton onClick={handleGoogleClick} fullWidth radius="xl">
            Google
          </GoogleButton>
          <MicrosoftButton onClick={handleMicrosoftClick} fullWidth radius="xl">
            Microsoft
          </MicrosoftButton>
          <GitHubButton onClick={handleGitHubClick} fullWidth radius="xl">
            GitHub
          </GitHubButton>
        </Group>
      </Paper>
    </Center>
  );
};
export default Login;
