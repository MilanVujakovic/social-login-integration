import axios, { AxiosResponse, AxiosError } from "axios";
import { useState, useEffect } from "react";
import { LOGIN_PAGE } from "../common/routes";

type Status = "pending" | "success" | "error";

type ResponseWithData<T> = {
  data: T;
  status: number;
  message: string;
};

const promiseWrapper = <T>(
  promise: Promise<AxiosResponse<ResponseWithData<T>>>
): T => {
  let status: Status = "pending";
  let result: AxiosResponse<ResponseWithData<T>> | AxiosError;

  const s = promise.then(
    (value) => {
      status = "success";
      result = value;
    },
    (error) => {
      status = "error";
      result = error;
    }
  );

  // @ts-expect-error no time to fix
  return () => {
    switch (status) {
      case "pending":
        throw s;
      case "success":
        if (result instanceof Error) {
          throw result;
        } else {
          return result.data.data; // Return result.data.data for success
        }
      case "error":
        if (result && "response" in result && result.response?.status === 401) {
          window.location.href = LOGIN_PAGE;
          throw result;
        } else {
          throw result;
        }
      default:
        throw new Error("Unknown status");
    }
  };
};

function useGetData<T>(url: string) {
  const [resource, setResource] = useState<T | null>(null);

  useEffect(() => {
    const getData = () => {
      const promise = axios.get<ResponseWithData<T>>(url, {
        withCredentials: true,
      });
      setResource(promiseWrapper(promise));
    };

    getData();
  }, [url]);

  return resource;
}

export default useGetData;
