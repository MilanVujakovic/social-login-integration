import "./App.css";

import "@mantine/core/styles.css";
import { Outlet } from "react-router-dom";

import { MantineProvider, createTheme } from "@mantine/core";
import { ErrorBoundary } from "react-error-boundary";
import ErrorPage from "./pages/ErrorPage/ErrorPage";

const theme = createTheme({
  fontFamily: "Montserrat, sans-serif",
  defaultRadius: "md",
});

function App() {
  return (
    <MantineProvider theme={theme}>
      <ErrorBoundary fallback={<ErrorPage />}>
        <Outlet />
      </ErrorBoundary>
    </MantineProvider>
  );
}

export default App;
