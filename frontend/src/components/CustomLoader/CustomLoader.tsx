import { Center, Container, Loader } from "@mantine/core";

const CustomLoader = () => {
  return (
    <Container size="xl" style={{ height: "100dvh" }}>
      <Center style={{ height: "100%" }}>
        <Loader size="xl" />
      </Center>
    </Container>
  );
};

export default CustomLoader;
