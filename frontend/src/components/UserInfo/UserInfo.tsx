import { Avatar, Paper, Text } from "@mantine/core";
import { UserData } from "../../types/User";

const UserInfo = ({ data }: { data: UserData }) => {
  const { email, image, fullname } = data;
  return (
    <Paper
      radius="md"
      withBorder
      p="lg"
      bg="var(--mantine-color-body)"
      style={{ border: "none" }}
    >
      <Avatar src={image} size={120} radius={120} mx="auto" />
      <Text ta="center" fz="lg" fw={500} mt="md">
        {fullname}
      </Text>
      <Text ta="center" c="dimmed" fz="sm">
        {email}
      </Text>
    </Paper>
  );
};

export default UserInfo;
