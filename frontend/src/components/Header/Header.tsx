import { useState } from "react";
import styles from "./Header.module.css";
import {
  ActionIcon,
  Avatar,
  Container,
  Group,
  Menu,
  Text,
  UnstyledButton,
  rem,
  useComputedColorScheme,
  useMantineColorScheme,
} from "@mantine/core";
import {
  IconChevronDown,
  IconLogout,
  IconMoon,
  IconSun,
} from "@tabler/icons-react";
import clsx from "clsx";
import { IconSocial } from "@tabler/icons-react";
import { UserData } from "../../types/User";

const Header = ({
  data,
  onLogout,
}: {
  data: UserData;
  onLogout: () => void;
}) => {
  const { image, fullname } = data;
  const [userMenuOpened, setUserMenuOpened] = useState(false);
  const { setColorScheme } = useMantineColorScheme();
  const computedColorScheme = useComputedColorScheme("light", {
    getInitialValueInEffect: true,
  });

  return (
    <div className={styles.header}>
      <Container className={styles.mainSection} size="xl">
        <Group justify="space-between">
          <IconSocial width={30} height={50} />
          <span>User info</span>
          <Group justify="space-between">
            <Menu
              width={260}
              position="bottom-end"
              transitionProps={{ transition: "pop-top-right" }}
              onClose={() => setUserMenuOpened(false)}
              onOpen={() => setUserMenuOpened(true)}
              withinPortal
            >
              <Menu.Target>
                <UnstyledButton
                  className={clsx(styles.user, {
                    [styles.userActive]: userMenuOpened,
                  })}
                >
                  <Group gap={7}>
                    <Avatar src={image} alt={fullname} radius="xl" size={20} />
                    <Text
                      miw={120}
                      maw={120}
                      fw={500}
                      size="sm"
                      lh={1.1}
                      mr={3}
                      lineClamp={1}
                    >
                      {fullname}
                    </Text>
                    <IconChevronDown
                      style={{ width: rem(12), height: rem(12) }}
                      stroke={1.5}
                    />
                  </Group>
                </UnstyledButton>
              </Menu.Target>
              <Menu.Dropdown>
                <Menu.Item
                  onClick={onLogout}
                  leftSection={
                    <IconLogout
                      style={{ width: rem(16), height: rem(16) }}
                      stroke={1.5}
                    />
                  }
                >
                  Logout
                </Menu.Item>
              </Menu.Dropdown>
            </Menu>
            <ActionIcon
              onClick={() =>
                setColorScheme(
                  computedColorScheme === "light" ? "dark" : "light"
                )
              }
              variant="default"
              size="l"
              aria-label="Toggle color scheme"
            >
              <IconSun
                className={clsx(styles.icon, styles.light)}
                stroke={1.5}
              />
              <IconMoon
                className={clsx(styles.icon, styles.dark)}
                stroke={1.5}
              />
            </ActionIcon>
          </Group>
        </Group>
      </Container>
    </div>
  );
};

export default Header;
