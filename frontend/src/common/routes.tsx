// API routes
export const API_GOOGLE_AUTH = "/auth/google";
export const API_MICROSOFT_AUTH = "/auth/microsoft";
export const API_GITHUB_AUTH = "/auth/github";

export const API_LOGOUT = "/auth/logout";

export const API_USER_DATA = "/users/data";

// Routes
export const LOGIN_PAGE = "/login";
