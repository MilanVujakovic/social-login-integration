import { PrismaClient, Provider, User } from "@prisma/client";
import { serialize } from "cookie";
import { Request, Response } from "express";
import jwt from "jsonwebtoken";
import { Profile as GoogleProfile } from "passport-google-oauth20";
import { AddUserData, UserData } from "../Interfaces/User";
import { Profile as GitHubProfile } from "passport-github2";

const prisma = new PrismaClient();

export const generateToken = (payload: any): string => {
  const token = jwt.sign(payload, process.env.JWT_SECRET || "secret", {
    expiresIn: parseInt(process.env.JWT_EXPIRATION || "3600", 10),
  });
  return token;
};

export const setTokenCookie = (res: Response, token: string): void => {
  const serialized = serialize("token", token, {
    httpOnly: true,
    secure: process.env.NODE_ENV === "production",
    // sameSite: 'strict',
    maxAge: parseInt(process.env.JWT_EXPIRATION || "3600", 10),
    path: "/",
  });
  res.setHeader("Set-Cookie", serialized);
};

export const googleLogin = async (
  req: Request,
  res: Response
): Promise<void> => {
  const { id, emails, displayName, photos } = req.user as GoogleProfile;
  const email = (emails && emails[0].value) || "";
  const userData: AddUserData = {
    email,
    provider: Provider.Google,
    providerUserId: id,
  };

  const user = await addUser(userData);

  const payload: UserData = {
    id: user.id,
    email: email,
    fullname: displayName,
    image: (photos && photos[0]?.value) || "",
  };

  const token = generateToken(payload);
  setTokenCookie(res, token);
  res.redirect(process.env.FRONT_URL || "");
};

export const microsoftLogin = async (
  req: Request,
  res: Response
): Promise<void> => {
  const { id, emails, displayName } = req.user as {
    id: string;
    emails: { type: string; value: string }[];
    displayName: string;
  };

  const email = (emails && emails[0].value) || "";
  const userData: AddUserData = {
    email,
    provider: Provider.Microsoft,
    providerUserId: id,
  };

  const user = await addUser(userData);
  const payload: UserData = {
    id: user.id,
    email: email,
    fullname: displayName,
    image: "",
  };
  const token = generateToken(payload);
  setTokenCookie(res, token);
  res.redirect(process.env.FRONT_URL || "");
};

export const gitHubLogin = async (
  req: Request,
  res: Response
): Promise<void> => {
  const { id, emails, displayName, photos, username } =
    req.user as GitHubProfile;

  const email = (emails && emails[0].value) || "";
  const userData: AddUserData = {
    email,
    provider: Provider.GitHub,
    providerUserId: id,
  };

  const user = await addUser(userData);
  const payload: UserData = {
    id: user.id,
    email: email,
    fullname: displayName || username || "",
    image: (photos && photos[0]?.value) || "",
  };
  const token = generateToken(payload);
  setTokenCookie(res, token);
  res.redirect(process.env.FRONT_URL || "");
};

export const addUser = async (data: AddUserData) => {
  if (!data.email) throw "No email";

  const user = await prisma.user.findUnique({
    where: {
      email: data.email,
    },
  });

  if (user) {
    return user;
  } else {
    const createUser = await prisma.user.create({ data });
    return createUser;
  }
};
