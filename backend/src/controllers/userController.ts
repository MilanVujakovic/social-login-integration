import { serialize } from "cookie";
import { NextFunction, Request, Response } from "express";

export const getUserData = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const data = req.user;

  res.status(200).json({
    data,
    status: "success",
    message: "Logged out",
  });
};
