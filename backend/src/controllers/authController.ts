import { serialize } from "cookie";
import { Request, Response } from "express";
import {
  gitHubLogin,
  googleLogin,
  microsoftLogin,
} from "../services/authService";

export const logout = async (req: Request, res: Response) => {
  const { cookies } = req;
  const jwt = cookies?.token;

  if (!jwt) {
    return res.status(200).json({
      status: "success",
      message: "Logged out",
    });
  }

  const serialized = serialize("token", "", {
    httpOnly: true,
    secure: process.env.NODE_ENV === "production",
    sameSite: "strict",
    maxAge: -1,
    path: "/",
  });
  res.setHeader("Set-Cookie", serialized);
  res.status(200).json({
    status: "success",
    message: "Logged out",
  });
};

export const googleLoginHandler = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    await googleLogin(req, res);
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ error: "Failed to log in" });
  }
};

export const microsoftLoginHandler = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    await microsoftLogin(req, res);
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ error: "Failed to log in" });
  }
};

export const gitHubLoginHandler = async (
  req: Request,
  res: Response
): Promise<void> => {
  try {
    await gitHubLogin(req, res);
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ error: "Failed to log in" });
  }
};
