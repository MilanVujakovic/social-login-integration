import passport from "passport";
import {
  Strategy as GoogleStrategy,
  Profile as GoogleProfile,
} from "passport-google-oauth20";
import { Strategy as MicrosoftStrategy } from "passport-microsoft";
import {
  Strategy as GitHubStrategy,
  Profile as GitHubProfile,
} from "passport-github2";
import axios from "axios";

passport.use(
  new GoogleStrategy(
    {
      callbackURL: process.env.API_URL + "/auth/google/callback",
      clientID: process.env.GOOGLE_CLIENT_ID || "",
      clientSecret: process.env.GOOGLE_CLIENT_SECRET || "",
    },
    async (
      accessToken: string,
      refreshToken: string,
      profile: GoogleProfile,
      done: any
    ) => {
      return done(null, profile);
    }
  )
);

passport.use(
  new MicrosoftStrategy(
    {
      callbackURL: process.env.API_URL + "/auth/microsoft/callback",
      clientID: process.env.MICROSOFT_CLIENT_ID || "",
      clientSecret: process.env.MICROSOFT_CLIENT_SECRET || "",
      tenant: "consumers",
      scope: ["user.read"],
    },
    async (
      accessToken: string,
      refreshToken: string,
      profile: any,
      done: any
    ) => {
      return done(null, profile);
    }
  )
);

passport.use(
  new GitHubStrategy(
    {
      clientID: process.env.GITHUB_CLIENT_ID || "",
      clientSecret: process.env.GITHUB_CLIENT_SECRET || "",
      callbackURL: process.env.API_URL + "/auth/github/callback",
      scope: ["user:email"],
    },
    function (
      accessToken: string,
      refreshToken: string,
      profile: GitHubProfile,
      done: any
    ) {
      return done(null, profile);
    }
  )
);

export default passport;
