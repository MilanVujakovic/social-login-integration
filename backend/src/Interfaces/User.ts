import { Provider } from "@prisma/client";

export interface AddUserData {
  email: string;
  provider: Provider;
  providerUserId: string;
}

export interface UserData {
  id: string;
  email: string;
  fullname: string;
  image: string;
}
