import { Request, Response } from "express";
import { Router } from "express";
import passport from "../configs/passportStrategies";
import {
  gitHubLoginHandler,
  googleLoginHandler,
  logout,
  microsoftLoginHandler,
} from "../controllers/authController";

const router = Router();

// Google
router.get(
  "/google",
  passport.authenticate("google", {
    scope: ["profile", "email"],
    session: false,
  })
);
router.get(
  "/google/callback",
  passport.authenticate("google", { failureRedirect: "/", session: false }),
  googleLoginHandler
);

// Microsoft
router.get(
  "/microsoft",
  passport.authenticate("microsoft", { session: false })
);
router.get(
  "/microsoft/callback",
  passport.authenticate("microsoft", {
    session: false,
    failureRedirect: "/",
  }),
  microsoftLoginHandler
);

// GitHub
router.get("/github", passport.authenticate("github", { session: false }));
router.get(
  "/github/callback",
  passport.authenticate("github", {
    session: false,
  }),
  gitHubLoginHandler
);

router.post("/logout", logout);

export default router;
