import { Router } from "express";
import authMiddleware from "../middleware/authMiddleware";
import { getUserData } from "../controllers/userController";

const router = Router();

router.get("/data", authMiddleware, getUserData);

export default router;
