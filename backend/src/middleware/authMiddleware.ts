import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const { cookies } = req;
  const token = cookies?.token;

  if (token) {
    jwt.verify(
      token,
      process.env.JWT_SECRET || "secret",
      (err: any, user: any) => {
        if (err) {
          return res.status(401).json({
            message: "Unauthorized",
          });
        }

        req.user = user;
        next();
      }
    );
  } else {
    res.status(401).json({
      message: "Unauthorized",
    });
  }
};

export default authMiddleware;
